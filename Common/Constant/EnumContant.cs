﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Constant
{
    public static class EnumContant
    {
        public enum HttpType
        {
            Get = 0,
            Post = 1
        }
    }
}
