﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class ScriptInfo
    {
        public int ID { get; set; }
        public string PageName { get; set; }
        public string Url { get; set; }
        public List<XpathInfo> xpathInfoList { get; set; }
    }
}
