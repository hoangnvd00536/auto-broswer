﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class TransactionEntity
    {
        public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public string OTPResult { get; set; }

    }
}
