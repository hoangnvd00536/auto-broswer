﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class SimEntity
    {
        public int ID { get; set; }
        public string ApiKey { get; set; }
        public string SimToken { get; set; }
        public int ServiceID { get; set; }
        public int SmsNumber { get; set; }
        public List<TransactionEntity> TransactionList { get; set; }

    }
}
