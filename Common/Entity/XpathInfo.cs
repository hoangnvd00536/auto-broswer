﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public class XpathInfo
    {
        public int ID { get; set; }
        public string Xpath { get; set; }
        public Action ActionNumb { get; set; }
        public string Value { get; set; }
        public int WaitingTime { get; set; }

        public enum Action
        {
            Click = 1,
            Input = 2,
            Scroll = 3
        }
    }
}
