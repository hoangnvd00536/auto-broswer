﻿using Newtonsoft.Json.Linq;
using System;

namespace Common.Utilities
{
    public static class RandomUtil
    {
        public static string RandomName()
        {
            var rd = new Random();
            var res = FileUtil.LoadJsonFile<object>("vietnamese_name_data.json");

            var firstNameArr = (JArray)JObject.Parse(Convert.ToString(res))["firstName"];
            var secondNameArr = (JArray)JObject.Parse(Convert.ToString(res))["secondName"];
            var lastNameArr = (JArray)JObject.Parse(Convert.ToString(res))["lastName"];
            var fullName = String.Concat(firstNameArr[rd.Next(0, firstNameArr.Count - 1)], " ",
                                          secondNameArr[rd.Next(0, secondNameArr.Count - 1)], " ",
                                          lastNameArr[rd.Next(0, lastNameArr.Count - 1)]);
            return fullName;
        }
    }
}