﻿
using Common.Entity;
using Microsoft.Win32;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using KAutoHelper;

namespace AutoBroswer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static ScriptInfo scriptInfo = new ScriptInfo();
        private List<IWebDriver> driverList = new List<IWebDriver>();
        List<IntPtr> windows = new List<IntPtr>();

        public MainWindow()
        {
            InitializeComponent();
            this.Xpath_GridData.ItemsSource = new List<XpathInfo>();
        }

        #region Event control

        /// <summary>
        /// Click Open script button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_Script_Btn_Click(object sender, RoutedEventArgs e)
        {
            scriptInfo = LoadScriptFile();
            this.Page_Name_Textbox.Text = scriptInfo.PageName;
            this.Page_Url_Textbox.Text = scriptInfo.Url;
            this.Xpath_GridData.ItemsSource = scriptInfo.xpathInfoList;
        }

        /// <summary>
        /// Mouse down in grid data cell event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xpath_GridData_MouseDown(object sender, MouseButtonEventArgs e)
        {
            return;
            var cell = (DataGridCell)sender;
            if (!(cell.DataContext.GetType().Equals(typeof(XpathInfo)))) return;
            var selected = (XpathInfo)cell.DataContext;

            this.Xpath_TextBox.Text = selected.Xpath;
            this.ID_TextBox.Text = selected.ID.ToString();
            this.WaitTime_TextBox.Text = selected.WaitingTime.ToString();
        }

        /// <summary>
        /// Click Add Xpath Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add_Xpath_Button_Click(object sender, RoutedEventArgs e)
        {
            var newXpath = new XpathInfo()
            {
                ID = Int32.Parse(this.ID_TextBox.Text),
                WaitingTime = Int32.Parse(this.WaitTime_TextBox.Text),
                Xpath = this.Xpath_TextBox.Text
            };
            this.InsertXpath(this.Xpath_GridData.SelectedIndex, this.Xpath_GridData.ItemsSource as List<XpathInfo>, newXpath);
        }

        /// <summary>
        /// Save file to local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            scriptInfo.PageName = this.Page_Name_Textbox.Text;
            scriptInfo.Url = this.Page_Url_Textbox.Text;
            this.SaveScript(scriptInfo);
        }

        /// <summary>
        /// Run script
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Run_Script_Button_Click(object sender, RoutedEventArgs e)
        {
            scriptInfo.Url = this.Page_Url_Textbox.Text;
            scriptInfo.xpathInfoList = this.Xpath_GridData.ItemsSource as List<XpathInfo>;
            var listThread = new List<Thread>();
            for (int i = 0; i < Int32.Parse(this.Thread_Number_Textbox.Text); i++)
            {
                // listThread.Add(new Thread(RunScript));
                // listThread[i].Start();
                RunScript();
            }
            listThread.ForEach(t => t.Join());
            MessageBox.Show("Done");
        }

        /// <summary>
        /// Close program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion Event control

        #region Function control

        /// <summary>
        /// Load Script From file
        /// </summary>
        /// <returns>Xpath Info list</returns>
        private ScriptInfo LoadScriptFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Environment.CurrentDirectory
            };

            if (openFileDialog.ShowDialog() == true)
            {
                var json = File.ReadAllText(openFileDialog.FileName);
                return JsonConvert.DeserializeObject<ScriptInfo>(json);
            }
            return null;
        }

        /// <summary>
        /// Insert new Xpath to specify index of list
        /// </summary>
        /// <param name="index"></param>
        /// <param name="xpathInfos"></param>
        private void InsertXpath(int index, List<XpathInfo> xpathInfos, XpathInfo newXpath)
        {
            if (index < 0)
            {
                MessageBox.Show("Please choose a row");
                return;
            }
            xpathInfos.Insert(index, newXpath);
            this.Xpath_GridData.ItemsSource = null;
            this.Xpath_GridData.ItemsSource = xpathInfos;
        }

        private void SaveScript(ScriptInfo scriptInfo)
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Title = "Save an Script File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                scriptInfo.xpathInfoList = this.Xpath_GridData.ItemsSource as List<XpathInfo>;

                using (StreamWriter file = new StreamWriter(saveFileDialog1.FileName))
                {
                    file.Write(JsonConvert.SerializeObject(scriptInfo, Formatting.Indented));
                }
            }
        }

        private void RunScript()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("no-sandbox");
            options.BinaryLocation = Path.Combine(Environment.CurrentDirectory, "chrome-win", "chrome.exe");
            var driver = new ChromeDriver(options);
            
            var windowHandle = IntPtr.Zero;

            while (true)
            {
                var listProcess = AutoControl.FindWindowHandlesFromProcesses(null, "data:, - Chromium");
                if (listProcess.Count > 0)
                {
                    windowHandle = listProcess[0];
                    driverList.Add(driver);
                    windows.Add(windowHandle);
                }
                
                if (windowHandle != IntPtr.Zero)
                {
                    break;
                }
                Thread.Sleep(500);
            }

            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(scriptInfo.Url);

            
            //Xpath selector click
            foreach (var xpath in scriptInfo.xpathInfoList)
            {
                switch (xpath.ActionNumb)
                {
                    case XpathInfo.Action.Click:
                        driver.FindElement(By.XPath(xpath.Xpath)).Click();
                        break;

                    case XpathInfo.Action.Input:
                        driver.FindElement(By.XPath(xpath.Xpath)).SendKeys(xpath.Value);
                        break;

                    case XpathInfo.Action.Scroll:
                        var element = driver.FindElement(By.XPath(xpath.Xpath));
                        Actions actions = new Actions(driver);
                        actions.MoveToElement(element);
                        actions.Perform();
                        break;

                    default:
                        break;
                }
                Thread.Sleep(xpath.WaitingTime);
            }
        }

        #endregion Function control

        private void Close_Broswer_Button_Click(object sender, RoutedEventArgs e)
        {
            if (driverList.Count > 0)
            {
                driverList.ForEach(d => d.Quit());
            }
        }
    }
}