﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTool.Constant
{
    public static class EnumConstant
    {
        public enum PostType
        {
            StatusPost = 1,
            CommentPost = 2
        }

        public enum CommentType
        {
            PostComment = 1,
            CommentInComment = 2
        }

        public enum CommentKind
        {
            Happy = 1,
            Sad = 2,
            Congratulation = 3,
            Dot = 4

        }
        public enum ReactType
        {
            Like = 1,
            Love = 2,
            LOL = 3,
            WOW = 4,
            Sad = 5,
            Angry = 6,
            Random = 0
        }
    }
}
