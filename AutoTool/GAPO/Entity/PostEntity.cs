﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutoTool.Constant.EnumConstant;

namespace AutoTool.Entity
{
    public class PostEntity
    {
        public PostType PostType { get; set; }
        public ReactType ReactType { get; set; }
    }
    
}
