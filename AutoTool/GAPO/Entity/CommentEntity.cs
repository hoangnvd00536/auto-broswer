﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutoTool.Constant.EnumConstant;

namespace AutoTool.Entity
{
    public class CommentEntity
    {
        public int ID { get; set; }
        public CommentType CommentType { get; set; }
        public CommentKind CommentKind { get; set; }
        public string Content { get; set; }
    }
}
