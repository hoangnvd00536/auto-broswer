﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GapoTool.Entity
{
    public class GapoAccount
    {
        public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string UserInfo { get; set; }
    }
}
