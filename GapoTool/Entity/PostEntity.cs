﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GapoTool.Constant.EnumConstant;

namespace GapoTool.Entity
{
    public class PostEntity
    {
        public PostType PostType { get; set; }
        public ReactType ReactType { get; set; }
    }
    
}
