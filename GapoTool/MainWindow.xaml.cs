﻿using Common.Constant;
using Common.Entity;
using Common.Utilities;
using GapoTool.Constant;
using GapoTool.Entity;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace GapoTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constant variant

        private readonly string CREATE_TRANS_API = "http://api.codesim.net/api/CodeSim/DangKy_GiaoDich?apikey={0}&dichvu_id={1}&so_sms_nhan={2}";
        private readonly string GET_TRANS_API = "http://api.codesim.net/api/CodeSim/KiemTraGiaoDich?apikey={0}&giaodich_id={1}";
        private readonly string API_KEY = "72f093f3-fe37-4210-919c-636b0e8d4a3f";
        private readonly string SIM_TOKEN = "zVj_LrEGCsBkpErxGpWhCkmvdImXWMhbhjKJ5QeRkCVYa-JlE33ZEKQYF7_i183UUlYpTUJNvUa4D9YrwisXIiK4FBIbGKp8_m3VeBIHVZSf0HTZ1KpyzlNLF2_bQafG8i71_WFim9icjMn8kgAeSP6yWeq1FoSVP8IjFIEExAGDLHSQShScAuWhlTfv4QSkkxEy2GQi4o7u8LS2lGXaQD62DdLyS6yIVIoiLn29Jzl_6BWesTRnH9c-2mIaikP4GbDseYBAMRuXbBBNzkWguQxXyYe1a6Rv7tayDck1zUhZLOFJN78Entm-Kx83rFbYPn7GlQ";
        private readonly int SERVICE_ID = 37;
        private readonly int SMS_NUMB = 1;
        private readonly string GAPO_SCRIPT = "gapo";
        private readonly string GAPO_ACCOUNT_PATH = "gapo_account.json";
        private readonly string GAPO_COMMENT_DATA_PATH = "gapo_comment_data.json";
        private readonly string GAPO_REACT_POST_API = "https://api.gapo.vn/react/v3.0/post/react/{0}";
        private readonly string GAPO_REACT_COMMENT_POST_API = "https://api.gapo.vn/react/v3.0/comment/react/{0}";
        private readonly string GAPO_FOLLOW_API = "https://api.gapo.vn/follow/v1.0/follow/pages";
        private readonly string GAPO_COMMENT_POST_API = "https://api.gapo.vn/main/v1.4/comment/create";

        #endregion Constant variant

        #region initial global variant

        public static SimEntity simEntity;
        private List<IWebDriver> driverList = new List<IWebDriver>();
        private List<GapoAccount> gapoAccounts;
        private List<CommentEntity> commentList;

        #endregion initial global variant

        /// <summary>
        /// Initial window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // initial value in UI

            this.React_Type_ComboBox.ItemsSource = Enum.GetValues(typeof(EnumConstant.ReactType));
            this.React_Type_ComboBox.SelectedItem = EnumConstant.ReactType.Random;
            this.Post_Type_ComboBox.ItemsSource = Enum.GetValues(typeof(EnumConstant.PostType));
            this.Post_Type_ComboBox.SelectedItem = EnumConstant.PostType.StatusPost;
            this.Comment_Type_ComboBox.ItemsSource = Enum.GetValues(typeof(EnumConstant.CommentType));
            this.Comment_Type_ComboBox.SelectedItem = EnumConstant.CommentType.PostComment;
            this.Comment_Kind_ComboBox.ItemsSource = Enum.GetValues(typeof(EnumConstant.CommentKind));
            this.Comment_Kind_ComboBox.SelectedItem = EnumConstant.CommentKind.Dot;

            // end initial value in UI

            simEntity = new SimEntity()
            {
                ApiKey = API_KEY,
                ServiceID = SERVICE_ID,
                SmsNumber = SMS_NUMB,
                SimToken = SIM_TOKEN,
                TransactionList = new System.Collections.Generic.List<TransactionEntity>()
            };

            gapoAccounts = FileUtil.LoadJsonFile<List<GapoAccount>>(GAPO_ACCOUNT_PATH);
            if (gapoAccounts == null)
            {
                gapoAccounts = new List<GapoAccount>();
            }
            this.Total_Account.Text = gapoAccounts.Count.ToString();
            commentList = FileUtil.LoadJsonFile<List<CommentEntity>>(GAPO_COMMENT_DATA_PATH);
            if (commentList == null)
            {
                commentList = new List<CommentEntity>();
            }

            this.Comment_DataGrid.ItemsSource = commentList;
        }

        #region App event handle

        /// <summary>
        /// Create account
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Register_Button_Click(object sender, RoutedEventArgs e)
        {
            var register_number = Int32.Parse(this.Register_Number_Textbox.Text);
            var listThread = new List<Thread>();
            var script = FileUtil.LoadJsonFile<ScriptInfo>(GAPO_SCRIPT);

            for (int k = 0; k < Int32.Parse(this.Thread_Number_Textbox.Text); k++)
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("no-sandbox");
                //options.BinaryLocation = Path.Combine(Environment.CurrentDirectory, "GoogleChromePortable", "GoogleChromePortable.exe");
                var driver = new ChromeDriver(options);
                driverList.Add(driver);
            }

            for (int j = 0; j < register_number;)
            {
                for (int i = 0; i < Int32.Parse(this.Thread_Number_Textbox.Text);)
                {
                    if (i == driverList.Count) break;
                    ChromeDriver driver = driverList[i] as ChromeDriver;
                    listThread.Add(new Thread(() => RegisterAccount(driver, script)));
                    listThread[i].Start();
                    i++;
                    if (++j == register_number) break;
                }
                listThread.ForEach(t => t.Join());
                listThread.Clear();
                FileUtil.SaveJsonFile(gapoAccounts, GAPO_ACCOUNT_PATH);
            }
            if (driverList.Count > 0)
            {
                driverList.ForEach(d => { d.Close(); d.Quit(); d.Dispose(); });
                driverList.Clear();
            }
            this.Total_Account.Text = gapoAccounts.Count.ToString();
            MessageBox.Show("Done");
        }

        private void Close_All_Broswer_Click(object sender, RoutedEventArgs e)
        {
            if (driverList.Count > 0)
            {
                driverList.ForEach(d => d.Dispose());
                driverList.Clear();
            }
        }

        private void Page_Id_Button_Click(object sender, RoutedEventArgs e)
        {
            string pageId = String.Empty;
            FollowPage(pageId);
        }

        private void REACT_BUTTON_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            HttpClient httpClient = new HttpClient();
            string currentPost = String.Empty;
            int currentReact = (int)this.React_Type_ComboBox.SelectedValue;
            if ((EnumConstant.PostType)this.Post_Type_ComboBox.SelectedItem == EnumConstant.PostType.StatusPost)
            {
                currentPost = GAPO_REACT_POST_API;
            }
            else
            {
                currentPost = GAPO_REACT_COMMENT_POST_API;
            }
            gapoAccounts.ForEach(g =>
            {
                Random r = new Random();
                RunHttpClient(String.Format(currentPost, this.POST_ID_TEXTBOX.Text), g.Token, EnumContant.HttpType.Post, "{\"react_type\":" + (currentReact == 0 ? r.Next(1, 7) : currentReact) + ",\"data_source\":1}");
                Console.WriteLine(++i);
            });
            httpClient.Dispose();
            MessageBox.Show(String.Format("Total: {0} reaction", gapoAccounts.Count));
        }

        private void Comment_Button_Click(object sender, RoutedEventArgs e)
        {
            var contentArr = commentList.FindAll(c => c.CommentKind == (EnumConstant.CommentKind)this.Comment_Kind_ComboBox.SelectedItem).Select(x => x.Content).ToArray();
            var commentId = this.Comment_Post_ID_TextBox.Text;
            var listThread = new List<Thread>();
            for (int j = 0; j < gapoAccounts.Count;)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (j == gapoAccounts.Count) break;
                    var random = new Random();
                    listThread.Add(new Thread(() => CommentPost(commentId, contentArr[random.Next(0, contentArr.Length - 1)], gapoAccounts[j].Token)));
                    listThread[i].Start();
                    if (++j == gapoAccounts.Count) break;
                }
                listThread.ForEach(t => t.Join());
                listThread.Clear();
                if (driverList.Count > 0)
                {
                    driverList.ForEach(d => d.Dispose());
                    driverList.Clear();
                }
            }
        }

        private void Save_Comment_Button_Click(object sender, RoutedEventArgs e)
        {
            commentList = this.Comment_DataGrid.ItemsSource as List<CommentEntity>;
            FileUtil.SaveJsonFile<List<CommentEntity>>(commentList, GAPO_COMMENT_DATA_PATH);
        }

        #endregion App event handle

        #region Function handle

        private GapoAccount GetAccount(IWebDriver driver, GapoAccount gapoAccount)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var userInfo = (String)js.ExecuteScript("return localStorage.getItem('userInfo')");
            var refresh_token = (String)js.ExecuteScript("return localStorage.getItem('refresh_token')");
            var token = (String)js.ExecuteScript("return localStorage.getItem('token')");
            gapoAccount.RefreshToken = refresh_token;
            gapoAccount.Token = token;
            gapoAccount.UserInfo = userInfo;
            return gapoAccount;
        }

        private void RegisterAccount(ChromeDriver driver, ScriptInfo scriptInfo)
        {
            try
            {
                HttpClient httpClient = new HttpClient();

                driver.Navigate().GoToUrl(scriptInfo.Url);
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("localStorage.clear()");
                js.ExecuteScript("sessionStorage.clear()");

                driver.Manage().Cookies.DeleteAllCookies();
                driver.Navigate().GoToUrl(scriptInfo.Url);

                var transaction = new TransactionEntity();
                GetPhoneNumber(httpClient, transaction);
                //CheckPhoneRegisted();
                // auto selenium
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[0].Xpath)).SendKeys(transaction.PhoneNumber);
                Thread.Sleep(scriptInfo.xpathInfoList[0].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[1].Xpath)).Click();
                Thread.Sleep(scriptInfo.xpathInfoList[1].WaitingTime);
                if (driver.FindElements(By.XPath(scriptInfo.xpathInfoList[2].Xpath)).Count == 0)
                {
                    return;
                }
                GetOTPNumber(httpClient, transaction);

                if (transaction.OTPResult == null)
                {
                    return;
                }
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[2].Xpath)).SendKeys(transaction.OTPResult);
                Thread.Sleep(scriptInfo.xpathInfoList[2].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[3].Xpath)).Click();
                Thread.Sleep(scriptInfo.xpathInfoList[3].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[4].Xpath)).SendKeys(scriptInfo.xpathInfoList[4].Value);
                Thread.Sleep(scriptInfo.xpathInfoList[4].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[5].Xpath)).SendKeys(scriptInfo.xpathInfoList[5].Value);
                Thread.Sleep(scriptInfo.xpathInfoList[5].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[6].Xpath)).Click();
                Thread.Sleep(scriptInfo.xpathInfoList[6].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[7].Xpath)).SendKeys(RandomUtil.RandomName());
                Thread.Sleep(scriptInfo.xpathInfoList[7].WaitingTime);
                driver.FindElement(By.XPath(scriptInfo.xpathInfoList[8].Xpath)).Click();
                Thread.Sleep(scriptInfo.xpathInfoList[8].WaitingTime);

                simEntity.TransactionList.Add(transaction);

                gapoAccounts.Add(GetAccount(driver, new GapoAccount() { PhoneNumber = transaction.PhoneNumber }));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
        }

        private void CheckPhoneRegisted()
        {
            throw new NotImplementedException();
        }

        private TransactionEntity GetOTPNumber(HttpClient httpClient, TransactionEntity transaction)
        {
            var i = 0;
            while (i < 10)
            {
                var response = httpClient.GetAsync(String.Format(GET_TRANS_API, simEntity.ApiKey, transaction.ID)).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    if (((JArray)result["data"]["listSms"]).Count > 0)
                    {
                        var listSms = ((JArray)result["data"]["listSms"]);
                        var otpContent = (string)listSms[0]["smsContent"];
                        var otpStr = Regex.Match(otpContent, @"\d+").Value.Substring(0, 4);
                        transaction.OTPResult = otpStr;
                        return transaction;
                    }
                }
                Thread.Sleep(2000);
                i++;
                continue;
            }
            return transaction;
        }

        private SimEntity GetPhoneNumber(HttpClient httpClient, TransactionEntity transaction)
        {
            var response = httpClient.GetAsync(String.Format(CREATE_TRANS_API, simEntity.ApiKey, simEntity.ServiceID, simEntity.SmsNumber)).Result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = JObject.Parse(response.Content.ReadAsStringAsync().Result);

                transaction.ID = (int)result["data"]["id_giaodich"];

                response = httpClient.GetAsync(String.Format(GET_TRANS_API, simEntity.ApiKey, transaction.ID)).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    transaction.PhoneNumber = (string)result["data"]["phoneNum"];
                }
            }
            return simEntity;
        }

        private void FollowPage(string pageId)
        {
            for (int i = gapoAccounts.Count - 1; i >= 0; i--)
            {
                var isOK = RunHttpClient(GAPO_FOLLOW_API, gapoAccounts[i].Token, EnumContant.HttpType.Post, "{\"entity_id\":\"" + this.Page_Id_Textbox.Text + "\",\"see_first\":0}");
                Console.WriteLine(1 - i / gapoAccounts.Count + "%");
                if (!isOK)
                {
                    Console.WriteLine("End account to follow");
                    break;
                }
            }
        }

        private bool RunHttpClient(string url, string token, EnumContant.HttpType httpType, string content = null, FormUrlEncodedContent formUrlEncodedContent = null)
        {
            bool isOK = true;
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage rs;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            if (httpType == EnumContant.HttpType.Get)
            {
                rs = httpClient.GetAsync(url).Result;
            }
            else
            {
                if (content != null)
                {
                    rs = httpClient.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json")).Result;
                }
                else
                {
                    rs = httpClient.PostAsync(url, formUrlEncodedContent).Result;
                }
            }
            if (!rs.IsSuccessStatusCode)
            {
                isOK = false;
            }
            httpClient.Dispose();
            return isOK;
        }

        private void CommentPost(string post_id, string content, string token)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("post_id", post_id),
                new KeyValuePair<string, string>("content", content)
            });

            RunHttpClient(GAPO_COMMENT_POST_API, token, EnumContant.HttpType.Post, formUrlEncodedContent: formContent);
        }

        #endregion Function handle

        private void Save_Account_Button_Click(object sender, RoutedEventArgs e)
        {
            var rdN = RandomUtil.RandomName();
            this.Page_Id_Textbox.Text = rdN;
        }
    }
}