﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using KAutoHelper;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace thaiProject
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var qList = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "answer_new.txt"))
                .Where(line => line.StartsWith("|"))
                .Select(line => line.Substring(1)) // Loại bỏ dấu '|' bằng cách lấy chuỗi từ vị trí thứ 1
                .ToList();
            var answerList = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "answer_new.txt"))
                .Where(line => line.StartsWith("-"))
                .Select(line => line.Substring(1)) // Loại bỏ dấu '|' bằng cách lấy chuỗi từ vị trí thứ 1
                .ToList();

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("no-sandbox");
            // chromeOptions.BinaryLocation = Path.Combine(Environment.CurrentDirectory, "chrome-win", "chrome.exe");
            chromeOptions.BinaryLocation =
                Path.Combine("G:\\Git_Project\\auto-broswer\\chrome", "chrome-win", "chrome.exe");
            int currentCount = 0;

            Open_Broser_Block:
            var driver = new ChromeDriver("G:\\Git_Project\\auto-broswer\\chrome", chromeOptions);
            var windowHandle = IntPtr.Zero;

            while (true)
            {
                var listProcess = AutoControl.FindWindowHandlesFromProcesses(null, "data:, - Chromium");

                if (listProcess.Count > 0)
                {
                    windowHandle = listProcess[0];
                }

                if (windowHandle != IntPtr.Zero)
                {
                    break;
                }

                Thread.Sleep(500);
            }

            AutoControl.MoveWindow(windowHandle, 10, 10, 1100, 768, true);
            driver.Url = "https://mttqhanoi.vn/login";
            driver.FindElement(By.XPath("//*[@id=\"username\"]")).SendKeys("0987879787");
            Thread.Sleep(500);
            driver.FindElement(By.XPath("//*[@id=\"password\"]")).SendKeys("123123");
            Thread.Sleep(500);
            driver.FindElement(
                    By.XPath("/html/body/content/div/div[2]/div/div/div/div/div[2]/div/div/form/div/div[4]/div/button"))
                .Click();
            Thread.Sleep(500);
            driver.Url = "https://mttqhanoi.vn/exams";
            Thread.Sleep(500);
            driver.FindElement(By.XPath("/html/body/content/div/section/div/div/div/div/button")).Click();

            var questionList = driver.FindElementsByClassName("questions_content");
            var questionContentList = driver.FindElementsByClassName("question-detail");
            Random random = new Random();
            for (int i = 0; i < questionList.Count; i = i + 4)
            {
                bool isClick = true;
                string answerStr = "";
                var question = questionContentList[i / 4].Text;
                // int randomNumber = random.Next(0, 4);
                int index = qList.IndexOf(question);
                if (index >= 0)
                {
                    if (answerList[index].ToLower()
                        .Equals(questionList[i].Text.ToLower()))
                    {
                        var answer = questionList[i].FindElement(By.XPath(".."));
                        answer.Click();
                        answerStr = questionList[i].Text;
                    }
                    else if (answerList[index].Equals(questionList[i + 1].Text))
                    {
                        var answer = questionList[i + 1].FindElement(By.XPath(".."));
                        answer.Click();
                        answerStr = questionList[i + 1].Text;
                    }
                    else if (answerList[index].Equals(questionList[i + 2].Text))
                    {
                        var answer = questionList[i + 2].FindElement(By.XPath(".."));
                        answer.Click();
                        answerStr = questionList[i + 2].Text;
                    }
                    else if (answerList[index].Equals(questionList[i + 3].Text))
                    {
                        var answer = questionList[i + 3].FindElement(By.XPath(".."));
                        answer.Click();
                        answerStr = questionList[i + 3].Text;
                    }
                    else
                    {
                        isClick = false;
                    }
                }
                else
                {
                    isClick = false;
                }


                if (!isClick)
                {
                    // Lưu lại câu chưa có đáp án
                    AddQuestion(
                        question, questionList[i].Text,
                        questionList[i + 1].Text,
                        questionList[i + 2].Text,
                        questionList[i + 3].Text
                    );
                }
                // else
                // {
                //     // Lưu lại câu có đáp án
                //     AddQuestionWithAnswer(
                //         question, answerStr
                //     );
                // }

                Thread.Sleep(300);
                driver.FindElementsByClassName("previous-question")[0].Click();
                Thread.Sleep(300);
            }

            driver.FindElementById("num_person").SendKeys("60000");

            Console.WriteLine("Done");
            Console.WriteLine("Bam nut bat ky de thoat");
            Console.ReadLine();
        }

        private static void AddQuestionWithAnswer(string question, string answerStr)
        {
            var lines = File.ReadLines(Path.Combine(Environment.CurrentDirectory, "question-answer.txt"))
                .Where(line => line.StartsWith("|"))
                .Select(line => line.Substring(1)) // Loại bỏ dấu '|' bằng cách lấy chuỗi từ vị trí thứ 1
                .ToList();
            if (!lines.Contains(question))
            {
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question-answer.txt"),
                    "|" + question + "\n");
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question-answer.txt"),
                    "-" + answerStr + "\n");
            }
        }

        private static void AddQuestion(string question, string s, string s1, string s2, string s3)
        {
            var lines = File.ReadLines(Path.Combine(Environment.CurrentDirectory, "question.txt"))
                .Where(line => line.StartsWith("|"))
                .Select(line => line.Substring(1)) // Loại bỏ dấu '|' bằng cách lấy chuỗi từ vị trí thứ 1
                .ToList();
            if (!lines.Contains(question))
            {
                // Thêm dòng vào cuối file
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question.txt"), "|" + question + "\n");
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question.txt"), "-" + s + "\n");
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question.txt"), "-" + s1 + "\n");
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question.txt"), "-" + s2 + "\n");
                File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "question.txt"), "-" + s3 + "\n");
            }
        }
    }
}