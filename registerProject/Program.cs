﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using KAutoHelper;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace registerProject
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("So tai khoan muon tao");
            int numb = 1;
            int.TryParse(Console.ReadLine(), out numb);
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("no-sandbox");
            // chromeOptions.AddExtension("mpbjkejclgfgadiemmefgebjfooflfhl-2.0.1-Crx4Chrome.com.crx");
            chromeOptions.AddExtension("G:\\Git_Project\\auto-broswer\\chrome\\mpbjkejclgfgadiemmefgebjfooflfhl-2.0.1-Crx4Chrome.com.crx");
            // chromeOptions.BinaryLocation = Path.Combine(Environment.CurrentDirectory, "chrome-win", "chrome.exe");
            chromeOptions.BinaryLocation =
                Path.Combine("G:\\Git_Project\\auto-broswer\\chrome", "chrome-win", "chrome.exe");
            int currentCount = 0;

            Open_Broser_Block:
            // var driver = new ChromeDriver(chromeOptions);
            var driver = new ChromeDriver("G:\\Git_Project\\auto-broswer\\chrome", chromeOptions);
            var windowHandle = IntPtr.Zero;

            while (true)
            {
                var listProcess = AutoControl.FindWindowHandlesFromProcesses(null, "data:, - Chromium");

                if (listProcess.Count > 0)
                {
                    windowHandle = listProcess[0];
                }

                if (windowHandle != IntPtr.Zero)
                {
                    break;
                }

                Thread.Sleep(500);
            }

            string phoneNumber = GetPhoneNumber();
            Console.WriteLine($"Process for phone number {phoneNumber}");

            AutoControl.MoveWindow(windowHandle, 10, 10, 1024, 768, true);
            driver.Url = "https://mttqhanoi.vn/";
            while (driver.Url != "https://mttqhanoi.vn/register")
            {
                Thread.Sleep(500);
            }
            Thread.Sleep(200);
            driver.FindElement(By.XPath("//*[@id=\"username\"]")).SendKeys(phoneNumber);
            Thread.Sleep(200);
            driver.FindElement(By.XPath("//*[@id=\"name\"]")).SendKeys(Common.Utilities.RandomUtil.RandomName());
            Thread.Sleep(200);
            driver.FindElement(By.XPath("//*[@id=\"password\"]")).SendKeys("123123");
            Thread.Sleep(200);
            driver.FindElement(By.XPath("//*[@id=\"password-confirm\"]")).SendKeys("123123");
            Thread.Sleep(200);
            var element = driver.FindElement(By.XPath("//*[@id=\"office_id\"]"));
            SelectElement dropDown = new SelectElement(element);
            dropDown.SelectByValue("39");
            Thread.Sleep(1000);

            // ResolveCaptcha(windowHandle);

            while (driver.Url != "https://mttqhanoi.vn/")
            {
                Thread.Sleep(500);
            }

            AppendPhoneNumber(phoneNumber);
            driver.Dispose();

            if (numb > ++currentCount)
            {
                goto Open_Broser_Block;
            }

            Console.WriteLine("Done!");
        }

        private static string GetPhoneNumber()
        {
            var phoneString = File.ReadLines(Path.Combine(Environment.CurrentDirectory, "phone.txt")).Last();
            var phone = long.Parse(phoneString);
            phone += 1;

            return ConvertPhoneNumber(phone.ToString(), false);
        }

        private static void AppendPhoneNumber(string phoneNumber)
        {
            phoneNumber = ConvertPhoneNumber(phoneNumber, true);
            // Thêm dòng vào cuối file
            File.AppendAllText(Path.Combine(Environment.CurrentDirectory, "phone.txt"), phoneNumber + "\n");
        }

        private static void ResolveCaptcha(IntPtr windowHandle)
        {
            StartCaptcha:
            ClickByImageOrImage(windowHandle, "captcha_image.png", "captcha_image_1.png", 1, 1000);
            Thread.Sleep(3000);
            ClickByImage(windowHandle, "solve_captcha.png", 1, 1000);


            // Kiếm tra xem catcha đã được veryfi hay chưa
            bool captchaOk = FindByImageNorImage(windowHandle, "captcha_ok.png", "reload_captcha.png", 5, 1000);

            if (captchaOk)
            {
                ClickByImage(windowHandle, "register_image.png", 1, 1000);
            }
            else
            {
                ClickByImage(windowHandle, "reload_captcha.png", 1, 1000);
                Thread.Sleep(1000);
                goto StartCaptcha;
            }
        }

        public static bool ClickByImage(IntPtr mainHandle, string subBmPath, int count = 3, int wait = 1000)
        {
            int currentCount = 0;
            var res = new System.Drawing.Point?();
            var subBitMap = ImageScanOpenCV.GetImage(subBmPath);
            while (true)
            {
                var captureHandle = (Bitmap)CaptureHelper.CaptureWindow(mainHandle);
                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap, 0.80);
                if (res.HasValue)
                {
                    AutoControl.SendClickOnPosition(mainHandle, res.Value.X, res.Value.Y);
                    return true;
                }

                if (++currentCount == count)
                {
                    return false;
                }

                Thread.Sleep(wait);
            }
        }

        public static bool ClickByImageOrImage(IntPtr windowHandle, string v1, string v2, int v3, int v4)
        {
            int currentCount = 0;
            var res = new System.Drawing.Point?();
            var subBitMap = ImageScanOpenCV.GetImage(v1);
            var subBitMap2 = ImageScanOpenCV.GetImage(v2);
            while (true)
            {
                var captureHandle = (Bitmap)CaptureHelper.CaptureWindow(windowHandle);
                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap);
                if (res.HasValue)
                {
                    AutoControl.SendClickOnPosition(windowHandle, res.Value.X, res.Value.Y);
                    return true;
                }

                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap2);
                if (res.HasValue)
                {
                    AutoControl.SendClickOnPosition(windowHandle, res.Value.X, res.Value.Y);
                    return true;
                }

                if (++currentCount == v3)
                {
                    return false;
                }

                Thread.Sleep(v4);
            }
        }

        public static Point? FindByImage(IntPtr mainHandle, string subBmPath, int count = 3, int wait = 1000)
        {
            int currentCount = 0;
            Point? res;
            var subBitMap = ImageScanOpenCV.GetImage(subBmPath);
            while (true)
            {
                var captureHandle = (Bitmap)CaptureHelper.CaptureWindow(mainHandle);
                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap);
                if (res.HasValue)
                {
                    AutoControl.SendClickOnPosition(mainHandle, res.Value.X, res.Value.Y);
                    return res;
                }

                if (++currentCount == count)
                {
                    return res;
                }

                Thread.Sleep(wait);
            }
        }

        internal static bool FindByImageNorImage(IntPtr mainHandle, string subBmPath, string subBmPath1, int count,
            int wait)
        {
            int currentCount = 0;
            var res = new System.Drawing.Point?();
            var subBitMap = ImageScanOpenCV.GetImage(subBmPath);
            var subBitMap1 = ImageScanOpenCV.GetImage(subBmPath1);
            while (true)
            {
                var captureHandle = (Bitmap)CaptureHelper.CaptureWindow(mainHandle);
                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap);
                if (res.HasValue)
                {
                    //AutoControl.SendClickOnPosition(mainHandle, res.Value.X, res.Value.Y);
                    return true;
                }

                res = ImageScanOpenCV.FindOutPoint(captureHandle, subBitMap1);
                if (res.HasValue)
                {
                    //AutoControl.SendClickOnPosition(mainHandle, res.Value.X, res.Value.Y);
                    return false;
                }

                if (++currentCount == count)
                {
                    return false;
                }

                Thread.Sleep(wait);
            }
        }

        static string ConvertPhoneNumber(string phoneNumber, bool toInternational)
        {
            // Nếu cần chuyển sang định dạng quốc tế (bắt đầu bằng "84")
            if (toInternational)
            {
                if (phoneNumber.StartsWith("0"))
                {
                    return "84" + phoneNumber.Substring(1);
                }
            }
            // Nếu cần chuyển về định dạng bắt đầu bằng "0"
            else
            {
                if (phoneNumber.StartsWith("84"))
                {
                    return "0" + phoneNumber.Substring(2);
                }
            }

            // Trả về số điện thoại gốc nếu không cần chuyển đổi
            return phoneNumber;
        }
    }
}